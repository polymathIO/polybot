FROM node:0.10-onbuild
# replace this with your application's default port
EXPOSE 8888

ENV HUBOT_HIPCHAT_JID 100803_1408356@chat.hipchat.com 
ENV HUBOT_HIPCHAT_PASSWORD 123qwe!@#QWE 
ENV HUBOT_HIPCHAT_ROOMS 100803_web_development_team@conf.hipchat.com 
# ENV HUBOT_HIPCHAT_ROOMS_BLACKLIST  
ENV HUBOT_HIPCHAT_JOIN_ROOMS_ON_INVITE true 
# ENV HUBOT_HIPCHAT_HOST conf.hipchat.com 
# ENV HUBOT_HIPCHAT_XMPP_DOMAIN  
ENV HUBOT_LOG_LEVEL debug 
# ENV HUBOT_HIPCHAT_RECONNEC  

RUN apt-get install -y libexpat1-dev
RUN apt-get install -y libicu-dev

CMD ["bin/hubot","--name","polybot","--adapter","hipchat"]